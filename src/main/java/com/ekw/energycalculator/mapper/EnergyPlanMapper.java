package com.ekw.energycalculator.mapper;

import com.ekw.energycalculator.model.*;
import org.springframework.stereotype.Component;

import javax.money.Monetary;
import javax.money.MonetaryAmount;

@Component
public class EnergyPlanMapper {
    private static final MonetaryAmount TURNOVER_LIMIT = Monetary.getDefaultAmountFactory().setCurrency("EUR").setNumber(1000000).create();

    public EnergyPlan getClientEnergyPlan(Client client){

        EnergyPlan energyPlan=null;
        if(client==null ){
            return energyPlan;
        }
        if(client  instanceof IndividualClient){
                energyPlan =new EnergyPlan(Energy.G.getIndividualClientRate(), Energy.E.getIndividualClientRate());
        } else if (client instanceof ProfessionalClient && ((ProfessionalClient) client).getAnnualTurnover().isGreaterThan(TURNOVER_LIMIT)) {
            energyPlan = new EnergyPlan(Energy.G.getProfessionalClientAboveOneMillionRate(), Energy.E.getProfessionalClientAboveOneMillionRate());
        }else {
            energyPlan = new EnergyPlan(Energy.G.getProfessionalClientUnderOneMillionRate(), Energy.E.getProfessionalClientUnderOneMillionRate());
        }
        return energyPlan;
    }
}
