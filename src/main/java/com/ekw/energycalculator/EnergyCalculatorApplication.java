package com.ekw.energycalculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnergyCalculatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnergyCalculatorApplication.class, args);
	}

}
