package com.ekw.energycalculator.service;

import com.ekw.energycalculator.mapper.EnergyPlanMapper;
import com.ekw.energycalculator.model.Invoice;
import com.ekw.energycalculator.model.Client;
import com.ekw.energycalculator.model.ClientEnergyConsumption;
import com.ekw.energycalculator.model.EnergyPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import java.time.YearMonth;

@Component
public class InvoiceService {

    @Autowired
    private EnergyPlanMapper energyPlanMapper;

    public Invoice computeClientMonthlyBill(Client client, ClientEnergyConsumption clientEnergyConsumption, YearMonth month){
        Invoice invoice = new Invoice(client.getId(), month);
        EnergyPlan energyPlan = energyPlanMapper.getClientEnergyPlan(client);
        MonetaryAmount amount = Monetary.getDefaultAmountFactory().setCurrency("EUR").setNumber(
                energyPlan.getGazPrice().getNumber().doubleValueExact()*clientEnergyConsumption.getGazConsumption().getNumberOfKwh()
                        + energyPlan.getElectricityPrice().getNumber().doubleValueExact()*clientEnergyConsumption.getElectricityConsumption().getNumberOfKwh()
        ).create();
        invoice.setAmount(amount);

        return invoice;
    }
}
