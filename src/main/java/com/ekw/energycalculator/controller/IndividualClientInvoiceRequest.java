package com.ekw.energycalculator.controller;

import com.ekw.energycalculator.model.IndividualClient;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@Data
@AllArgsConstructor
public class IndividualClientInvoiceRequest {

    @Valid
    private IndividualClient client;
    private double gazUnits;
    private double electricityUnits;
    private int year;
    private int month;
}
