package com.ekw.energycalculator.controller;

import com.ekw.energycalculator.model.Client;
import com.ekw.energycalculator.model.ClientEnergyConsumption;
import com.ekw.energycalculator.model.IndividualClient;
import com.ekw.energycalculator.model.Invoice;
import com.ekw.energycalculator.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.YearMonth;

@RestController
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    @PostMapping(path = "/clients/individual/invoice",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ClientInvoiceResponse getIndividualClientInvoice(@RequestBody IndividualClientInvoiceRequest individualClientInvoiceRequest){

        YearMonth month= YearMonth.of(individualClientInvoiceRequest.getYear(),individualClientInvoiceRequest.getMonth());
        IndividualClient client=individualClientInvoiceRequest.getClient();

        return clientInvoiceResponseFromInvoice(invoiceService.computeClientMonthlyBill(client
                , new ClientEnergyConsumption(client.getId(),
                        month,individualClientInvoiceRequest.getGazUnits(),individualClientInvoiceRequest.getElectricityUnits()),month));

    }

    @PostMapping(path = "/clients/professional/invoice",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ClientInvoiceResponse getProfessionalClientInvoice(@RequestBody @Valid  IndividualClientInvoiceRequest individualClientInvoiceRequest){

        YearMonth month= YearMonth.of(individualClientInvoiceRequest.getYear(),individualClientInvoiceRequest.getMonth());
        IndividualClient client=individualClientInvoiceRequest.getClient();

        return clientInvoiceResponseFromInvoice(invoiceService.computeClientMonthlyBill(client
                , new ClientEnergyConsumption(client.getId(),
                        month,individualClientInvoiceRequest.getGazUnits(),individualClientInvoiceRequest.getElectricityUnits()),month));

    }

    private ClientInvoiceResponse clientInvoiceResponseFromInvoice(Invoice invoice){
        return new ClientInvoiceResponse(invoice.getAmount().getNumber().doubleValueExact()+" "+ invoice.getAmount().getCurrency().getCurrencyCode(),invoice.getMonth(),invoice.getClientId());
    }



}
