package com.ekw.energycalculator.controller;

import com.ekw.energycalculator.model.IndividualClient;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProfessionalClientInvoiceRequest {

    private ProfessionalClientInvoiceRequest client;
    private double gazUnits;
    private double electricityUnits;
    private int year;
    private int month;
}
