package com.ekw.energycalculator.controller;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.YearMonth;

@Data
@AllArgsConstructor
public class ClientInvoiceResponse {

    private String amount;
    private YearMonth month;
    private String clientId;
}
