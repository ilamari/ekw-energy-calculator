package com.ekw.energycalculator.model;

import lombok.*;

@Getter
@Data
public class EnergyConsumption {
    @NonNull
    private Energy energy;
    @NonNull
    private double numberOfKwh;

    public EnergyConsumption(@NonNull Energy energy, @NonNull double numberOfKwh) {
        this.energy = energy;
        this.numberOfKwh = numberOfKwh;
    }

    public void setNumberOfKwh(double numberOfKwh) {
        this.numberOfKwh = numberOfKwh;
    }
}
