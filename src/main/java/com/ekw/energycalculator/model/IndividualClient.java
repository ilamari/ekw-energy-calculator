package com.ekw.energycalculator.model;

import lombok.*;

import javax.validation.constraints.Pattern;

@Getter
public class IndividualClient extends Client{
    @NonNull
    private String firstName;
    @NonNull
    private String lastName;
    @NonNull
    private Civility civility;

    public IndividualClient(@NonNull @Pattern(regexp = "^EKW\\d{8}$") String id, @NonNull String firstName, @NonNull String lastName, @NonNull Civility civility) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.civility = civility;
    }
}
