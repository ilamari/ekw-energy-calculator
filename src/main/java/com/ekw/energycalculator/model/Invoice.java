package com.ekw.energycalculator.model;

import lombok.Getter;
import lombok.NonNull;

import javax.money.MonetaryAmount;
import java.time.YearMonth;


@Getter
public class Invoice {
    @NonNull
    private YearMonth month;
    @NonNull
    private MonetaryAmount amount;
    @NonNull
    private String clientId;

    public Invoice(@NonNull String clientId, @NonNull YearMonth month) {
        this.clientId = clientId;
        this.month = month;
    }

    public void setAmount(MonetaryAmount amount) {
        this.amount = amount;
    }
}
