package com.ekw.energycalculator.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.money.MonetaryAmount;

@AllArgsConstructor
@Getter
public class EnergyPlan {
    private MonetaryAmount gazPrice;
    private MonetaryAmount electricityPrice;
}
