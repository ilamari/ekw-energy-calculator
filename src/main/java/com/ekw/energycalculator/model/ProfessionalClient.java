package com.ekw.energycalculator.model;

import lombok.*;

import javax.money.MonetaryAmount;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;


@Setter
@Getter
@Valid
public class ProfessionalClient extends Client{
    @NonNull
    @Pattern(regexp = "^\\d{9}$",message = "SIRET is 9 digit identifier")
    private String companyId;
    @NonNull
    private String companyName;
    @NonNull
    private MonetaryAmount annualTurnover;

    public ProfessionalClient(@NonNull @Pattern(regexp = "^(EKW)(\\d{8})$") String id, @NonNull String companyId, @NonNull String companyName, @NonNull MonetaryAmount annualTurnover) {
        super(id);
        this.companyId = companyId;
        this.companyName = companyName;
        this.annualTurnover = annualTurnover;
    }
}
