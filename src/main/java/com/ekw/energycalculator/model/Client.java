package com.ekw.energycalculator.model;


import lombok.Data;
import lombok.NonNull;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;


@Data
@Valid
public class Client {

    @NonNull
    @Pattern(regexp = "^(EKW)(\\d{8})$", message = "EKW identifier has the format: EKW + 8 digits, example: EKW12345643")
    private final String id;
}
