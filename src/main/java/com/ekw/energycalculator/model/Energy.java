package com.ekw.energycalculator.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;
import javax.money.Monetary;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.ExchangeRateProvider;
import javax.money.convert.MonetaryConversions;


@Getter
@AllArgsConstructor
public enum Energy {
    E ("Electricity",
            Monetary.getDefaultAmountFactory().setCurrency("EUR").setNumber(0.121).create(),
            Monetary.getDefaultAmountFactory().setCurrency("EUR").setNumber(0.114).create(),
            Monetary.getDefaultAmountFactory().setCurrency("EUR").setNumber(0.118).create()
    ),
    G("Gaz",
            Monetary.getDefaultAmountFactory().setCurrency("EUR").setNumber(0.115).create(),
            Monetary.getDefaultAmountFactory().setCurrency("EUR").setNumber(0.111).create(),
            Monetary.getDefaultAmountFactory().setCurrency("EUR").setNumber(0.113).create()
    );
    private final String type;
    private final MonetaryAmount individualClientRate;
    private final MonetaryAmount professionalClientAboveOneMillionRate;
    private final MonetaryAmount professionalClientUnderOneMillionRate;
}