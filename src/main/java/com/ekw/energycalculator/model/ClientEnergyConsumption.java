package com.ekw.energycalculator.model;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.time.YearMonth;


@Getter
@Setter
public class ClientEnergyConsumption {
    @NonNull
    private EnergyConsumption electricityConsumption;
    @NonNull
    private EnergyConsumption gazConsumption;

    @NonNull
    private YearMonth month;

    @NonNull
    private String clientId;

    public ClientEnergyConsumption(String clientId, YearMonth month) {
        this.clientId=clientId;
        this.month=month;
        this.electricityConsumption = new EnergyConsumption(Energy.E, 0);
        this.gazConsumption = new EnergyConsumption(Energy.G, 0);
    }

    public ClientEnergyConsumption(String clientId, YearMonth month,double gazNumberOfUnits, double electricityNumberOfUnits) {
        this(clientId,month);
        setElectricityConsumption(electricityNumberOfUnits);
        setGazConsumption(gazNumberOfUnits);
    }

    public void setGazConsumption(double numberOfUnits) {
        this.gazConsumption.setNumberOfKwh(numberOfUnits);
    }

    public void setElectricityConsumption(double numberOfUnits) {
        this.electricityConsumption.setNumberOfKwh(numberOfUnits);
    }
}
