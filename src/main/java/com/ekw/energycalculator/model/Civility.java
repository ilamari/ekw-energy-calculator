package com.ekw.energycalculator.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Civility {
    M ("Monsieur"), MME("Madame");
    private final String value;

}
