package com.ekw.energycalculator.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.money.Monetary;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ProfessionalClientTest {

    private static Validator validator;

    @BeforeAll
    public static void setupValidatorInstance() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }


    @Test
    void shouldNotSetIdToNull() {
        Exception exception = assertThrows(NullPointerException.class, () -> {
            new ProfessionalClient(null, "123447453", "awesome company",
                    Monetary.getDefaultAmountFactory().setCurrency("EUR").setNumber(1500000).create()
            );
        });

        String expectedMessage = "id is marked non-null but is null";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void shouldNotSetIdIfNotEkwFormat() {
        // Given
        Set<ConstraintViolation<ProfessionalClient>> violations;

        // When
        violations = validator.validate(new ProfessionalClient("EKW1234753", "665378545", "awesome company",
                Monetary.getDefaultAmountFactory().setCurrency("EUR").setNumber(1500000).create()
        ));
        // Then
        assertEquals(1, violations.size());

        assertThat(violations.stream().findFirst().map(ConstraintViolation::getMessage).orElse(""))
                .isEqualTo("EKW identifier has the format: EKW + 8 digits, example: EKW12345643");
    }

    @Test
    void shouldNotSetCompanyNameToNull() {
        Exception exception = assertThrows(NullPointerException.class, () -> {
            new ProfessionalClient("EKW123467536", "123427453", null,
                    Monetary.getDefaultAmountFactory().setCurrency("EUR").setNumber(1500000).create()
            );
        });

        String expectedMessage = "companyName is marked non-null but is null";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void shouldNotSetCompanyIdToNull() {
        Exception exception = assertThrows(NullPointerException.class, () -> {
            new ProfessionalClient("EKW123467536", null, "awesome company",
                    Monetary.getDefaultAmountFactory().setCurrency("EUR").setNumber(1500000).create()
            );
        });

        String expectedMessage = "companyId is marked non-null but is null";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void shouldNotSetCompanyIdIfNotSiret() {
        // Given
        Set<ConstraintViolation<ProfessionalClient>> violations;

        // When
        violations = validator.validate(new ProfessionalClient("EKW12347536", "6653", "awesome company",
                Monetary.getDefaultAmountFactory().setCurrency("EUR").setNumber(1500000).create()
        ));
        // Then
        assertEquals(1, violations.size());

        assertThat(violations.stream().findFirst().map(ConstraintViolation::getMessage).orElse(""))
                .isEqualTo("SIRET is 9 digit identifier");
    }


}