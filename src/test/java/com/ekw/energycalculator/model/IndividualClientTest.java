package com.ekw.energycalculator.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class IndividualClientTest {


    private static Validator validator;

    @BeforeAll
    public static void setupValidatorInstance() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    void shouldNotSetIdToNull() {
        Exception exception = assertThrows(NullPointerException.class, () -> {
            new IndividualClient(null, "John", "Smith", Civility.M);
        });

        String expectedMessage = "id is marked non-null but is null";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void shouldNotSetIdIfNotEkwFormat() {
        // Given
        Set<ConstraintViolation<IndividualClient>> violations;

        // When
        violations = validator.validate(new IndividualClient("EKW65365", "John", "Smith", Civility.M));
        // Then
        assertEquals(1,violations.size());

        assertThat(violations.stream().findFirst().map(ConstraintViolation::getMessage).orElse(""))
                .isEqualTo("EKW identifier has the format: EKW + 8 digits, example: EKW12345643");
    }

    @Test
    void shouldNotSetFirstNameToNull() {
        Exception exception = assertThrows(NullPointerException.class, () -> {
            new IndividualClient("EKW12345634", null, "Smith", Civility.M);
        });

        String expectedMessage = "firstName is marked non-null but is null";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void shouldNotSetLastNameToNull() {
        Exception exception = assertThrows(NullPointerException.class, () -> {
            new IndividualClient("EKW12345634", "John", null, Civility.M);
        });

        String expectedMessage = "lastName is marked non-null but is null";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void shouldNotSetCivilityToNull() {
        Exception exception = assertThrows(NullPointerException.class, () -> {
            new IndividualClient("EKW12345634", "John", "Smith", null);
        });

        String expectedMessage = "civility is marked non-null but is null";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }
}