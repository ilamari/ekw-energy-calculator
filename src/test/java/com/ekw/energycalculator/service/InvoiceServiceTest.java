package com.ekw.energycalculator.service;

import com.ekw.energycalculator.model.*;
import org.junit.jupiter.api.Test;

import javax.money.Monetary;
import java.time.YearMonth;

import static org.junit.jupiter.api.Assertions.*;

class InvoiceServiceTest {

    @Test
    void shouldComputeIndividualClientMonthlyBill() {
        Client client= new IndividualClient("EKW12345634","John","Smith", Civility.M);
        YearMonth month= YearMonth.now();
        ClientEnergyConsumption clientEnergyConsumption= new ClientEnergyConsumption(client.getId(), month,60, 70);
        InvoiceService invoiceService =new InvoiceService();
        Invoice invoice = invoiceService.computeClientMonthlyBill(client,clientEnergyConsumption,YearMonth.now());
        assertNotNull(invoice);
        assertEquals(invoice.getClientId(),client.getId());
        assertEquals(invoice.getMonth(),month);
    }

    @Test
    void shouldComputeProfessionalClientAboveOneMillionMonthlyBill() {
        Client client= new ProfessionalClient("EKW12345634","123447453","awesome company",
                Monetary.getDefaultAmountFactory().setCurrency("EUR").setNumber(1500000).create()
                );
        YearMonth month= YearMonth.now();
        ClientEnergyConsumption clientEnergyConsumption= new ClientEnergyConsumption(client.getId(), month,1850, 700);
        InvoiceService invoiceService =new InvoiceService();
        Invoice invoice = invoiceService.computeClientMonthlyBill(client,clientEnergyConsumption,YearMonth.now());
        assertNotNull(invoice);
        assertEquals(invoice.getClientId(),client.getId());
        assertEquals(invoice.getMonth(),month);
    }

    @Test
    void shouldComputeProfessionalClientUnderOneMillionMonthlyBill() {
        Client client= new ProfessionalClient("EKW546789234","123447453","awesome small company",
                Monetary.getDefaultAmountFactory().setCurrency("EUR").setNumber(500000).create()
        );
        YearMonth month= YearMonth.now();
        ClientEnergyConsumption clientEnergyConsumption= new ClientEnergyConsumption(client.getId(), month,1850, 700);
        InvoiceService invoiceService =new InvoiceService();
        Invoice invoice = invoiceService.computeClientMonthlyBill(client,clientEnergyConsumption,YearMonth.now());
        assertNotNull(invoice);
        assertEquals(invoice.getClientId(),client.getId());
        assertEquals(invoice.getMonth(),month);
    }
}